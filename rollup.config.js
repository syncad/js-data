import babel from 'rollup-plugin-babel'

export default {
  moduleName: 'JSData',
  external: [ 'big.js' ],
  globals: {
    'big.js': 'Big'
  },
  amd: {
    id: 'js-data'
  },
  plugins: [
    babel({
      babelrc: false,
      plugins: [
        'external-helpers'
      ],
      presets: [
        [
          'es2015',
          {
            modules: false
          }
        ]
      ],
      exclude: 'node_modules/**'
    })
  ]
}
